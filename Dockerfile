FROM archlinux:latest
WORKDIR /root

RUN pacman -Sy --noconfirm archiso git device-mapper

COPY build-files/build-iso.sh .

ENTRYPOINT ["bash", "build-iso.sh"]