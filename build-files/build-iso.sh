#!/usr/bin/env bash

PROFILE_DIR="profile"
PROFILE_URL="https://gitlab.com/omnitix/elyos/elyos-profile"

rename_iso() {
    cd iso &&
        mv install-elyos--x86_64.iso install-elyos-x86_64.iso &&
        cd ..
}

git clone --depth=1 $PROFILE_URL $PROFILE_DIR &&
    cd $PROFILE_DIR &&
    time mkarchiso -v -o iso . &&
    rename_iso &&
    mv iso/*.iso /tmp &&
    exit 0

exit 1