#!/usr/bin/env bash

IMGNAME="elyos/install-img"

docker build --rm -t $IMGNAME .

if [ $? -eq 0 ]; then
    docker run --rm -v /tmp:/tmp -ti --privileged $IMGNAME
else
    echo "Error building Docker image"
    exit 1
fi
